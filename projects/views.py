from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import ProjectForm
from tasks.models import Task


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project_details = Project.objects.get(id=id)
    context = {"project_details": project_details}
    return render(request, "projects/detail.html", context)


@login_required
def show_task(request, id):
    tasks = Task.objects.get(id=id)
    context = {"tasks": tasks}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
